<?php

namespace App\Serializer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class ValidatorDenormalizer implements DenormalizerInterface
{

    public function __construct( private ObjectNormalizer $normalizer, private ValidatorInterface $validator)
    {}
	
	public function denormalize(mixed $data, string $type, string $format = null, array $context = array()) {
        $errors = new ConstraintViolationList();
        $reflection = new \ReflectionClass($type);
        $props =$reflection->getProperties();
        foreach ($props as $prop) {
            $value = null;
            if(isset($data[$prop->name])) {
                $value = $data[$prop->name];
            }
            $errors->addAll($this->validator->validatePropertyValue($type,$prop->name,$value));

        }
        if($errors->count() > 0) {
            throw new ValidationFailedException('Validation error', $errors);
        }
        return $this->normalizer->denormalize($data,$type,$format,$context);
	}
	
	public function supportsDenormalization(mixed $data, string $type, string $format = null) {
        return true;
	}
}
