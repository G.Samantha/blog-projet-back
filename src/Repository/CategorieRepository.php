<?php
namespace App\Repository;

use App\Entities\Categorie;
use PDO;

class CategorieRepository
{
    private PDO $connection;
    public function __construct()
    {
        $this->connection = Database::connect();
    }

    /**
     * Affiche tous les categories
     * @return array
     */
    public function findAll(): array
    {
        $categorie = [];
        $statement = $this->connection->prepare('SELECT * FROM session');
        $statement->execute();
        $result = $statement->fetchAll();

        foreach ($result as $key) {
            $categorie[] = new Categorie($key['name'], $key['id']);
        }

        return $categorie;
    }

    /**
     * Affiche la categorie correspondant à l'id selectionné
     * @param int $id
     * @return Categorie|null
     */
    public function findById(int $id): Categorie|null
    {
        $statement = $this->connection->prepare('SELECT * FROM categorie WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $results = $statement->fetch();
        if ($results) {
            return new Categorie($results['name'], $results['id']);
        }

        return null;
    }

    /**
     * Ajout categorie à la base de donnée
     * @param categorie $categorie
     * @return void
     */
    public function persist(Categorie $categorie)
    {
        $statement = $this->connection->prepare('INSERT INTO categorie (name) VALUES (:name)');
        $statement->bindValue('name', $categorie->getName());

        $statement->execute();
        
        $categorie->setId($this->connection->lastInsertId());
    }

    /**
     * Permet d'update la categorie via l'id
     * @param int $id
     * @param string $name
     * @return void
     */
    public function update(int $id, string $name)
    {
        $statement = $this->connection->prepare('UPDATE categorie SET name = :name  WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->bindValue("name", $name);

        $statement->execute();
    }

    /**
     * Delete la categorie par l'ID
     * @param Categorie $categorie
     * @return void
     */
    public function delete(Categorie $categorie)
    {
        $statement = $this->connection->prepare('DELETE FROM categorie WHERE id=:id');
        $statement->bindValue('id', $categorie->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

}