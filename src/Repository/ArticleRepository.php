<?php
namespace App\Repository;

use App\Entities\Article;
use App\Entities\Categorie;
use PDO;
use DateTime;

class ArticleRepository {
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }


    /**
     * Affiche tous les articles
     * @return array
     */
    public function findAll() : array {

        $article = [];
        $statement = $this->connection->prepare('SELECT * FROM article');
        $statement->execute();
        $result = $statement->fetchAll();

        foreach ($result as $value) {
            $article[] = $this->sqlToArticle($value);
        }

        return $article;   
    }

    /**
     * Affiche l'article correspondant à l'id selectionné
     * @param int $id
     * @return Article|null
     */
    public function findById(int $id): Article|null
    {
        $statement = $this->connection->prepare('SELECT * FROM article WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $results = $statement->fetch();
        $date = null;
        if (isset($results['date'])) {
            $date = new DateTime($results['date']);
        }
        if ($results) {
            return $this->sqlToArticle($results);
        }

        return null;
    }

    /**
     * Ajout article à la base de donnée
     * @param Article $article
     * @return void
     */
    public function persist(Article $article) {

        $statement = $this->connection->prepare('INSERT INTO article (name, content, date, author, img) VALUES (:name, :content, :date, :author, :img)');
        $statement->bindValue('name', $article->getName());
        $statement->bindValue('content', $article->getContent());
        $statement->bindValue('date', $article->getDate()->format('Y-m-d'));
        $statement->bindValue('author', $article->getAuthor());
        $statement->bindValue('img', $article->getImg());

        $statement->execute();
        
        $article->setId($this->connection->lastInsertId());
    }

    /**
     * Permet d'update l'article via l'id
     * @param int $id
     * @param string $name
     * @param string $content
     * @param DateTime $date
     * @param string $author
     * @param string $img
     * @return void
     */
    public function update(Article $article){
        $statement = $this->connection->prepare('UPDATE article SET name=:name, content=:content,date=:date, author=:author, img=:img WHERE id=:id');

        $statement->bindValue(':name', $article->getName(), PDO::PARAM_STR);
        $statement->bindValue(':content', $article->getContent(), PDO::PARAM_STR);
        $statement->bindValue(':date', $article->getDate()->format('Y-m-d'), PDO::PARAM_STR);
        $statement->bindValue(':author', $article->getAuthor(), PDO::PARAM_STR);
        $statement->bindValue(':img', $article->getImg(), PDO::PARAM_STR);
        $statement->bindValue(':id', $article->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Delete l'article par l'ID
     * @param Article $article
     * @return void
     */
    public function delete(Article $article) {
        $statement = $this->connection->prepare('DELETE FROM article WHERE id=:id');
        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);

        $statement->execute();

    }

    private function sqlToArticle(array $line):Article {
        $date = null;
        if(isset($line['date'])){
            $date = new DateTime($line['date']);
        }
        return new Article($line['name'], $line['content'], $date, $line['author'], $line['img'], $line['id']);
    }


}