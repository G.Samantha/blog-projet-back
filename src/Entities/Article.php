<?php

namespace App\Entities;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

class Article {

    private ?int $id;
	#[Assert\NotBlank]
    private ?string $name;
	#[Assert\NotBlank]
    private ?string $content;
	#[Assert\Date]
    private ?DateTime $date;
    private ?string $author;
    private ?string $img;

    /**
     * @param int|null $id
     * @param string|null $name
     * @param string|null $content
     * @param DateTime|null $date
     * @param string|null $author
     * @param string|null $img
     */
    public function __construct(?string $name, ?string $content, ?DateTime $date, ?string $author, ?string $img, ?int $id=null) {
    	$this->id = $id;
    	$this->name = $name;
    	$this->content = $content;
    	$this->date = $date;
    	$this->author = $author;
    	$this->img = $img;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param string|null $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getContent(): ?string {
		return $this->content;
	}
	
	/**
	 * @param string|null $content 
	 * @return self
	 */
	public function setContent(?string $content): self {
		$this->content = $content;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getAuthor(): ?string {
		return $this->author;
	}
	
	/**
	 * @param string|null $author 
	 * @return self
	 */
	public function setAuthor(?string $author): self {
		$this->author = $author;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getImg(): ?string {
		return $this->img;
	}
	
	/**
	 * @param string|null $img 
	 * @return self
	 */
	public function setImg(?string $img): self {
		$this->img = $img;
		return $this;
	}
}