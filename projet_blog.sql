-- Active: 1673947756043@@127.0.0.1@3306@projet_blog



DROP TABLE IF EXISTS article_categorie;
DROP TABLE IF EXISTS categorie;
DROP TABLE IF EXISTS article;

CREATE TABLE
article (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (255) NOT NULL,
    content VARCHAR (10000),
    date DATE,
    author VARCHAR (255),
    img VARCHAR (1000)
);

CREATE TABLE
categorie (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (255) NOT NULL,
    id_article INT,
    FOREIGN KEY (id_article) REFERENCES article(id)
);

CREATE TABLE article_categorie (
    id_article INT,
    id_categorie INT,
    PRIMARY KEY (id_article, id_categorie),
    FOREIGN KEY (id_article) REFERENCES article(id),
    FOREIGN KEY (id_categorie) REFERENCES categorie (id)
);

INSERT INTO article (name, content,date,author,img) VALUES
('Fido','Fido le corgi est la mascotte de la promo 23. Il a été le model de toute une classe.','2021-04-24', 'Rana', 
'https://images.pexels.com/photos/4445618/pexels-photo-4445618.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1'),
('Jean-Marc','Jean-Marc est le favoris de Judith, doux, possedant un pelage brillant et magnifique','2021-04-24', 'Rana', 
'https://images.pexels.com/photos/236622/pexels-photo-236622.jpeg'),
('Rex','Le petit rigolo de la promo, il est toujours de bonne humeur, indispensable pour détendre les élèves','2021-04-24', 'Rana', 
'https://images.pexels.com/photos/3104708/pexels-photo-3104708.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1'),
('Joseph','Joseph est une charmante dame ? Qui vous motivera toujours avec joie et bonne humeur. Elle vous proposera souvent un verre avec de tequila et des tacos, malgré sa carrure imposante, on dit aussi qu\elle possède une botte secrète afin de disparaitre en un éclair. 
Si un jour dans les couloirs, vous entendrez ce crie "nigerundayo", c\est que Joseph utilise sa botte secrète.','2021-09-24', 'Araki', 
'https://i.pinimg.com/736x/66/0e/cb/660ecb2f3e53eea0f82922494ad08333.jpg'),
('Juan Pablo','Juan Pablo est le dieu du dev, son intelligence vous éblouira tel un soleil de midi. ','2021-04-24', 'Dalaï-Lama', 
'https://images.pexels.com/photos/9946105/pexels-photo-9946105.jpeg?auto=compress&cs=tinysrgb&w=600');

INSERT INTO categorie (name) VALUES
('Montagneux'), ('Artique'), ('Désert');